"""
This command line tool takes a directory path, an optional project code,
and an optional second directory path.

1. If only a project code is passed, the files in the first directory are
renamed and replace their project code with the new one.
2. If a project code and a second directory path is passed, copy the files into
the second directory path and only replace the project code in the new
directory.
3. If only a second directory is passed, make a copy of the files of the first
directory into the second.
4. If the given second directory does not exist, it is created.
5. If only an origin directory is given, it gives the full path of the directory.
"""


import shutil
import os.path
import argparse
import sys


def replace_prj_code(dir: str, old_code: str, new_code: str) -> None:
    """
    Takes a directory and replaces all files and directory names that include
    the org_code with the new_code

    Args:
        dir (str): the directory for which to change project file codes
        old_code (str): the old project code
        new_code (str): the new project code
    """

    for entry in os.listdir(dir):
        old_path = os.path.join(dir, entry)
        new_path = old_path.replace(old_code, new_code)
        os.rename(old_path, new_path)
        if os.path.isdir(new_path):
            replace_prj_code(new_path, old_code, new_code)


def rename_options(from_dir: str, to_dir: str, old_code: str, new_code: str) -> None:
    """
    Project codes are replaced and/or copied based on what arguments are given

    Args:
        from_dir (str): origin project directory
        to_dir (str): destination project directory
        old_code (str): old project code / base directory
        new_code (str): new project code
    """
    if new_code is None and to_dir is None:
        print(os.path.abspath(from_dir))
        exit()
    elif new_code is not None and to_dir is None:
        new_dir = from_dir.replace(old_code, new_code)
        os.rename(from_dir, new_dir)
        replace_prj_code(new_dir, old_code, new_code)
    elif to_dir is not None and new_code is None:
        shutil.copytree(from_dir, to_dir)
    else:
        if os.path.basename(to_dir) != new_code:
            sys.exit(
                "file_rename_and_copy.py: valueError: The project folder does not "
                + "match the given project code."
            )
        shutil.copytree(from_dir, to_dir)
        replace_prj_code(to_dir, old_code, new_code)


def main(from_dir: str, new_code: str, to_dir: str) -> None:
    """
    This command line tool takes a directory path, an optional project code,
    and an optional second directory path.
    
    Args:
        from_dir (str): an existing origin directory
        new_code (str): an optional new project code to replace old project code
        to_dir (str): an optional new destination directory
    """
    if not os.path.isdir(from_dir):
        sys.exit("file_rename_and_copy.py: valueError: Origin directory does not exist.")

    old_code = os.path.basename(from_dir)
    rename_options(from_dir, to_dir, old_code, new_code)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Renames and/or copies file names with given project code",
    )
    parser.add_argument(
        "from_dir",
        metavar="origin directory",
        help="an existing directory for which you want to change the "
        + "project code for",
    )
    parser.add_argument(
        "-p",
        "--prj_code",
        metavar="project code",
        help="new project code to rename files to",
    )
    parser.add_argument(
        "-d",
        "--to_dir",
        metavar="destination directory",
        help="the destination directory, new or existing, to where to"
        + "move or copy the files",
    )
    args = parser.parse_args()
    main(args.from_dir, args.prj_code, args.to_dir)
