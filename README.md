# file_rename_and_copy

file_rename_and_copy renames and/or copies a project directory with a new project code

## **Usage**

```
> python src/file_rename_and_copy.py [-h] origin_directory [-p project_code] [-d destination_directory] 
```
- [Rename a project and its contents with a new project code](#rename-a-project-and-its-contents-with-a-new-project-code)
- [Copy the contents of a project into a new directory](#copy-the-contents-of-a-project-into-a-new-directory)
- [Copy then rename a project and its contents with a new project code](#copy-then-rename-a-project-and-its-contents-with-a-new-project-code)
---

## Rename a project and its contents with a new project code

```
> python src/file_rename_and_copy.py projects/ahs -p [new code]
> python src/file_rename_and_copy.py projects/ahs -p abc
```

The contents of `projects/ahs`...

```
projects/
    ahs/
        ahs101/
            ahs101_randomshot_v001.jpg
            ahs101_randomshot_v002.jpg
        ahs102
```

...would be renamed to the following:

```
projects/
    abc/
        abc101/
            abc101_randomshot_v001.jpg
            abc101_randomshot_v002.jpg
        abc102
```
---

## Copy the contents of a project into a new directory

```
> python src\file_rename_and_copy.py [original directory] -d [new directory]
> python src\file_rename_and_copy.py projects/ahs -d ahsbackup
```

The contents of `projects/ahs` would be copied into `projects/ahsbackup`

```
projects/
    ahs/
        ahs101/
            ahs101_randomshot_v001.jpg
            ahs101_randomshot_v002.jpg
        ahs102
    ahsbackup/
        ahs101/
            ahs101_randomshot_v001.jpg
            ahs101_randomshot_v002.jpg
        ahs102
```
---
## Copy then rename a project and its contents with a new project code
```
> python file_rename_and_copy.py [origin directory] -p [new code] -d [new directory]
> python file_rename_and_copy.py projects/ahs -p qwe -d projects/qwe
```

The contents of `projects/ahs` would be copied into `projects/qwe` then renamed with the new project code

```
projects/
    ahs/
        ahs101/
            ahs101_randomshot_v001.jpg
            ahs101_randomshot_v002.jpg
        ahs102
    qwe/
        qwe101/
            qwe101_randomshot_v001.jpg
            qwe101_randomshot_v002.jpg
        qwe102
```